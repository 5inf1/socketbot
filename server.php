<?php

/*Pierini Luca 5info1 10/10/2014*/
/*Breve spiegazione del programma:
questo � un server in php che risponde al client (connesso con netcat); se in modalit� "normale", risponde ripetendo il ricevuto. In modalit� normale accetta anche quit e shutdown per chiudere o terminare.
Scrivendo "entrare" (ovviamente dal client) si accede alla command mode, nella quale possono essere eseguiti comandi sul server e l'output stampato sul client.
Scrivendo "uscire" si esce dalla command mode e si ritorna alla modalit� normale.
error_reporting(E_ALL);

/* Allow the script to hang around waiting for connections. */
set_time_limit(0);

/* Turn on implicit output flushing so we see what we are getting as it comes in. */
ob_implicit_flush();

$address = '127.0.0.1';	//indirizzo di connessione
$port = 10000;	//porta di connessione

	if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false)	//creazione del socket
		{
			echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";	//riporta l'eventuale errore
		} else {
			echo "Socket created. \n";	//riporta l'eventuale successo
		}
		
	if (socket_bind($sock, $address, $port) === false)	//binding del socket
		{
			echo "socket_bind() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";	//riporta l'eventuale errore
		} else {
			echo "Socket binded. \n";	//riporta l'eventuale successo
		}
		
	if (socket_listen($sock, 5) === false)	//ascolto del socket
		{
			echo "socket_listen() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";	//riporta l'eventuale errore
		} else {
			echo "Socket listening. \n";	//riporta l'eventuale successo
		}
	echo "Server started.\n";
	do {
		if (($msgsock = socket_accept($sock)) === false)	//accettazione del socket
			{
				echo "socket_accept() failed: reason: " . socket_strerror(socket_last_error($sock)) . "\n";	//riporta l'eventuale errore
				break;
			} else {
				echo "Socket accepted. \n";	//riporta l'eventuale successo
			}
			$msg = "\nWelcome to the PHP Test Server. \n" . "To quit, type 'quit'. To shut down the server type 'shutdown'. Type 'entrare' to enter command mode.\n";
			socket_Write($msgsock, $msg, strlen($msg));	//scrittura del benvenuto sul client
			
		do {	//ciclo principale
			if (false === ($buf = socket_read($msgsock, 2048, PHP_NORMAL_READ))) {	//si mette in ascolto sul socket
				echo "socket_read() failed: reason  " . socket_strerror(socket_last_error($msgsock)) . "\n";	//eventualmente restituisce l'errore
				break 2;	//e esce da tutto
			}
			if (!$buf = trim($buf)) {
				continue;
			}
			if ($buf == 'quit') {	//se quit chiude
				break;
			}
			if ($buf == 'shutdown') {	//se shutdown spegne
				socket_close($msgsock);
				break 2;
			}
			$finito = false;
			if ($buf == 'entrare') {	//se entrare entra nella command mode
				echo "Client entered the command mode.\n";	//benvenuto sul server
				$messaggio = "\nYou entered command mode. Type command to be excecuted on server. Type 'uscire' to exit this mode.\n";
				socket_write($msgsock, $messaggio, strlen($messaggio));	//benvenuto sul client
				do {	//ciclo della command mode
					$comandi = socket_read($msgsock, 2048, PHP_NORMAL_READ);	//si mette in ascolto
					if (!$comandi = trim($comandi)) {
						continue;
					}
					if ($comandi != 'uscire') {	//se diverso da uscire
						echo "Client send this command: " . $comandi . "\n";	//log sul server
						$messaggioo = shell_exec($comandi);	//esegue il comand
						socket_write($msgsock, "\n" . $messaggioo . "\n");	//resistuisce l'output sul client
					} else {	//se uguale a uscire
						echo "Client exit the command mode.\n";	//log sul server
						socket_write($msgsock, "You exit the command mode.\n", 28);	//log sul client
						$finito = true;	//sentinella a true
					}
				} while ($finito == false);	//ciclo con condizione sulla sentinella
			} else {	//se non � n� quit n� shutdown n� entrare
				$talkback = "PHP: You said '$buf'.\n";	//ripete quello che hai detto
				socket_write($msgsock, $talkback, strlen($talkback));	//lo scrive sul client
				echo "Client said $buf\n";	//lo scrive sul server
			}
		} while (true);
		socket_close($msgsock);
	} while (true);

/*<!--		if ($buf == 'dir') {
			$messaggio = shell_exec('dir');
			socket_write($msgsock, '\n' . $messaggio . '\n');
		}-->*/
socket_close($sock);
?>